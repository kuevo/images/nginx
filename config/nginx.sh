#!/bin/sh

set -e

# Create the www.conf
envsubst "$(env | sed -e 's/=.*//' -e 's/^/$/g')" < /etc/nginx/conf.d/$PROJECTTYPE.conf.tmpl > /etc/nginx/conf.d/www.conf

# If $DOCUMENT_ROOT env is set, and is NOT webroot, change it
if [ ! -z "$DOCUMENT_ROOT" ] && [ "$DOCUMENT_ROOT" != "webroot" ]; then
    sed -i "s|root /app/webroot;|root /app/$DOCUMENT_ROOT;|g" /etc/nginx/conf.d/www.conf
fi

/usr/sbin/nginx -c /etc/nginx/nginx.conf
