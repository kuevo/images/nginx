# syntax=docker/dockerfile:1.4
FROM bitnami/minideb:latest

# Run as root
USER root

# Install base packages
RUN \
    apt update && \
    apt upgrade -y && \
    install_packages \
        software-properties-common \
        gettext-base \
        patch \
        wget \
        curl \
    && \
    curl -sSLo /usr/share/keyrings/deb.sury.org-nginx.gpg https://packages.sury.org/nginx/apt.gpg && \
    sh -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-nginx.gpg] https://packages.sury.org/nginx/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/nginx.list' && \
    apt update && \
    install_packages \
        nginx \
    && \
    mkdir -p /var/cache/nginx

# Set workdir
WORKDIR /app

# Copy in production configs
COPY --link config/nginx.conf /etc/nginx/nginx.conf

# Copy in the various templates
COPY --link templates/* /etc/nginx/conf.d/

# Copy in entrypoint
COPY --link config/entrypoint.sh /entrypoint.sh
COPY --link config/nginx.sh /entrypoint-nginx.sh

# Make entrypoints executable
RUN chmod +x /entrypoint.sh /entrypoint-nginx.sh

WORKDIR /app
EXPOSE 80
ENV PROJECTTYPE="magento"
ENTRYPOINT [ "/entrypoint.sh" ]
CMD [ "/entrypoint-nginx.sh" ]
